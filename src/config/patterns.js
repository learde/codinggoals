const internet = {
  text: "Как работает интернет",
  value: {
    title: "Узнать как работает интернет",
    endCriterion: "Закрыть все пункты, узнав особенности работы интернета",
    description:
      '<h4>Как работает интернет?</h4><p>Изучая web-технологии, непременно необходимо начать с основ работы интернета.&nbsp;<br>Необходимо почитать про HTTP, DNS, хостинги, доменные имена. Это необходимо как в начале изучения front-end, так и back-end<br><br><a href="https://roadmap.sh"><i>задействована opensource roadmap.sh (здесь же можно найти полезные ресурсы)</i></a></p>',
    todo: [
      { title: "Как работает интернет", checked: 0 },
      { title: "Что такое DNS", checked: 0 },
      { title: "Что такое HTTP", checked: 0 },
      { title: "Браузеры и как они работают", checked: 0 },
      { title: "Что такое хостинг", checked: 0 },
      { title: "Что такое доменное имя", checked: 0 },
    ],
    isPrivate: false,
    isCommentable: true,
    isFinished: false,
  },
};

const html = {
  text: "HTML",
  value: {
    title: "Изучить HTML",
    endCriterion: "Закрыть все пункты, изучив основы языка разметки HTML",
    description:
      '<h4>Язык разметки HTML</h4><p>И front-end, и back-end разработчику необходимо знать основы HTML. Если путь дальше ляжет в сторону front-end разработки, придется изучать язык разметки более углубленно. На данном этапе я приступаю к изучению HTML, ресурсы и некоторое описание задач можно посмотреть по ссылке ниже.<br><br><a href="https://roadmap.sh"><i>задействована opensource roadmap.sh (здесь же можно найти полезные ресурсы)</i></a></p>',
    todo: [
      { title: "Самые основы", checked: 0 },
      { title: "Доступность (Accessibility)", checked: 0 },
      { title: "Основые SEO", checked: 0 },
      { title: "Семантика", checked: 0 },
      { title: "Формы и валидация", checked: 0 },
      { title: "Соглашения и лучшие практики (best practices)", checked: 0 },
    ],
    isPrivate: false,
    isCommentable: true,
    isFinished: false,
  },
};

const css = {
  text: "CSS",
  value: {
    title: "Изучить CSS",
    endCriterion: "Закрыть все пункты, изучив основы таблиц стилей CSS",
    description:
      '<h4>Таблица стилей CSS</h4><p>И front-end, и back-end разработчику необходимо знать основы CSS. Если путь дальше ляжет в сторону front-end разработки, придется изучать таблицы стилей более углубленно. На данном этапе я приступаю к изучению CSS, ресурсы и некоторое описание задач можно посмотреть по ссылке ниже.<br><br><a href="https://roadmap.sh"><i>задействована opensource roadmap.sh (здесь же можно найти полезные ресурсы)</i></a></p>',
    todo: [
      { title: "Самые основы", checked: 0 },
      { title: "Разметка (Layout)", checked: 0 },
      { title: "Отзывчивая, адаптивная верстка и медиа запросы", checked: 0 },
    ],
    isPrivate: false,
    isCommentable: true,
    isFinished: false,
  },
};

const jsBasic = {
  text: "Основы JavaScript",
  value: {
    title: "Изучить основы JavaScript",
    endCriterion:
      "Закрыть все пункты, изучив основы языка программирования JavaScript",
    description:
      '<h4>Основы JavaScript</h4><p>Для front-end разработчиков жизненно необходимо знать язык программирования JavaScript. Это самый часто используемый язык программирования, к тому же, единственный язык программирования, который работает напрямую в браузере. Этот язык позволит “оживить” вебстраницы, добавить интерактивных элементов, прослушивать взаимодействия пользователя. Более того, JS используется и для мобильной разработки<br><br><a href="https://roadmap.sh"><i>задействована opensource roadmap.sh (здесь же можно найти полезные ресурсы)</i></a></p>',
    todo: [
      { title: "Синтаксис и базовые конструкции", checked: 0 },
      { title: "Изучение объекта DOM", checked: 0 },
      { title: "Fetch и Ajax запросы", checked: 0 },
      { title: "Стандарт ES6 и выше (по крайней мере хорошо бы)", checked: 0 },
    ],
    isPrivate: false,
    isCommentable: true,
    isFinished: false,
  },
};

const git = {
  text: "Система контроля версий Git",
  value: {
    title: "Изучить основы git",
    endCriterion:
      "Закрыть все пункты, научившись пользоваться системой контроля версий git",
    description:
      '<h4>Система контроля версия Git</h4><p><strong>Система управления версиями</strong> (также используется определение «система контроля версий», от <i>Version Control System, VCS</i> или <i>Revision Control System</i>)&nbsp;— программное обеспечение для облегчения работы с изменяющейся информацией. Система управления версиями позволяет хранить несколько версий одного и того же документа, при необходимости возвращаться к более ранним версиям, определять, кто и когда сделал то или иное изменение, и многое другое. (<a href="https://ru.wikipedia.org/wiki/%D0%A1%D0%B8%D1%81%D1%82%D0%B5…D0%B8%D1%8F_%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%8F%D0%BC%D0%B8">wiki</a>)</p><p>Любому разработчику необходимо знать, что такое git, как с ним работать, к тому же, уметь пользоваться с сервисом github (gitlab, bitbucket и другие). Без git невозможно представить командную работу, значит, в любой вакансии всегда будет указано требование по умолчанию: git.&nbsp;</p><p>Изучить основы (и при должном усердии, даже углубленные моменты) git\'а можно на ресурсе <a href="https://githowto.com/ru"><i><strong>githowto</strong></i></a><br><br><a href="https://roadmap.sh"><i>задействована opensource roadmap.sh (здесь же можно найти полезные ресурсы)</i></a></p>',
    todo: [
      { title: "Пройти хотя бы часть курса на githowto", checked: 0 },
      { title: "Пройти весь курс githowto(хорошо было бы)", checked: 0 },
      {
        title: "Сделать первый репозиторий, выложить репозиторий на github",
        checked: 0,
      },
      {
        title: "Изучить другие ресурсы по типу github'a (gitlab, bitbucket)",
        checked: 0,
      },
    ],
    isPrivate: false,
    isCommentable: true,
    isFinished: false,
  },
};

const patterns = [internet, html, css, jsBasic, git];

export default patterns;
