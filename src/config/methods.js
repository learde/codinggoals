function init(Vue, store) {
  Vue.prototype.$getCookie = function (name) {
    const match = document.cookie.match(
      new RegExp("(^| )" + name + "=([^;]+)")
    );
    if (match) return match[2];
  };

  Vue.prototype.$setCookie = function (name, value, expire) {
    let date = new Date(Date.now() + expire * 1000);
    date = date.toUTCString();
    const domain = window.location.hostname;
    document.cookie =
      name + "=" + value + "; path=/; expires=" + date + "; domain=" + domain;
  };

  Vue.prototype.$isAuth = function () {
    const jwt = Vue.prototype.$getCookie("jwt");
    return jwt !== undefined && jwt !== null;
  };

  Vue.prototype.$refreshToken = function () {
    store.dispatch("refreshUser");
  };

  Vue.prototype.$startLoading = function () {
    store.commit("setLoading", true);
  };

  Vue.prototype.$stopLoading = function () {
    store.commit("setLoading", false);
  };

  Vue.prototype.$fillFormData = function (formData, obj) {
    for (let key in obj) {
      formData.append(key, obj[key]);
    }
  };
}

const methods = {
  init,
};

export default methods;
