import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/validate/index.js";
import methods from "@/config/methods";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";
import CKEditor from "@ckeditor/ckeditor5-vue2";

import { VBTooltipPlugin } from "bootstrap-vue";
Vue.use(VBTooltipPlugin);

const options = {
  confirmButtonColor: "#0069d9",
};

Vue.use(VueSweetalert2, options);
Vue.use(CKEditor);

Vue.config.productionTip = false;
methods.init(Vue, store);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
