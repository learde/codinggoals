import { extend } from "vee-validate";
import * as rules from "vee-validate/dist/rules";

Object.keys(rules).forEach((rule) => {
  extend(rule, rules[rule]);
});

// Override the default message.
extend("email", {
  ...rules.email,
  message: "Некорректный формат почты",
});

extend("required", {
  ...rules.required,
  message: "Поле обязательно для заполнения",
});

extend("max", {
  ...rules.max,
  message: (_, { length }) => {
    if (length % 10 === 1) return `Максимум ${length} символ`;
    else if (
      (length % 10 === 2 || length % 10 === 3 || length % 10 === 4) &&
      Math.floor(length / 10) % 10 !== 1
    )
      return `Максимум ${length} символа`;
    else return `Максимум ${length} символов`;
  },
});

extend("min", {
  ...rules.min,
  message: (_, { length }) => {
    if (length % 10 === 1) return `Минимум ${length} символ`;
    else if (
      (length % 10 === 2 || length % 10 === 3 || length % 10 === 4) &&
      Math.floor(length / 10) % 10 !== 1
    )
      return `Минимум ${length} символа`;
    else return `Минимум ${length} символов`;
  },
});

extend("password", {
  params: ["target"],
  validate(value, { target }) {
    return value === target;
  },
  message: "Пароли не совпадают",
});
