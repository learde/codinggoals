import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Login from "@/views/Login.vue";
import Registration from "@/views/Registration.vue";
import EditProfile from "@/views/EditProfile.vue";
import Profile from "@/views/Profile.vue";
import App from "@/views/App.vue";
import MyGoals from "@/views/MyGoals.vue";
import CreateGoal from "@/views/CreateGoal.vue";
import Goal from "@/views/Goal.vue";
import EditGoal from "@/views/EditGoal.vue";
import Favourites from "@/views/Favourites";
import Messages from "@/views/Messages";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "App",
    component: App,
  },
  {
    path: "/landing",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/registration",
    name: "Registration",
    component: Registration,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/profile/:id",
    name: "ProfileId",
    component: Profile,
    props: true,
  },
  {
    path: "/profile-edit",
    name: "EditProfile",
    component: EditProfile,
  },
  {
    path: "/goals/my",
    name: "MyGoals",
    component: MyGoals,
  },
  {
    path: "/goals/create",
    name: "CreateGoal",
    component: CreateGoal,
  },
  {
    path: "/goal/:id",
    name: "ViewGoal",
    component: Goal,
    props: true,
  },
  {
    path: "/goal/edit/:id",
    name: "EditGoal",
    component: EditGoal,
    props: true,
  },
  {
    path: "/favourites",
    name: "FavouriteGoals",
    component: Favourites,
  },
  {
    path: "/messages",
    name: "Messages",
    component: Messages,
  },
  {
    path: "/messages/:id",
    props: true,
    name: "MessagesUser",
    component: Messages,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const isUnAuthRoutes = to.name === "Login" || to.name === "Registration";
  if (Vue.prototype.$isAuth() && (isUnAuthRoutes || to.name === "Home"))
    next({ name: "App" });
  else if (!Vue.prototype.$isAuth() && (!isUnAuthRoutes || to.name === "Home"))
    next({ name: "Login" });
  else next();
});

export default router;
