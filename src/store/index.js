import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isAuth: false,
    user: null,
    hideContent: false,
    loading: false,
    profileItem: null,
    goalsItems: [],
    allGoals: [],
    profileGoals: [],
    favouritesGoals: [],
    theme: "light",
    sidebarMode: false,
    isMobile: false,
  },
  mutations: {
    setIsMobile(state, data) {
      state.isMobile = data;
    },
    setLoading(state, data) {
      state.loading = data;
    },
    setSidebarMode(state, data) {
      if (data !== undefined) {
        state.sidebarMode = data;
      } else {
        state.sidebarMode = !state.sidebarMode;
      }
    },
    setTheme(state, data) {
      state.theme = data;
    },
    setUser(state, data) {
      state.user = data;
    },
    setAuth(state, data) {
      state.isAuth = data;
    },
    setProfileItem(state, data) {
      state.profileItem = data;
    },
    setHideContent(state, data) {
      state.hideContent = data;
    },
    setGoalsItems(state, data) {
      state.goalsItems = data;
    },
    setProfileGoals(state, data) {
      state.profileGoals = data;
    },
    setAllGoals(state, data) {
      state.allGoals = data;
    },
    setFavouritesGoals(state, data) {
      state.favouritesGoals = data;
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getProfileItem(state) {
      return state.profileItem;
    },
  },
  actions: {
    authUser({ commit }, data) {
      return new Promise((resolve, reject) => {
        commit("setHideContent", true);
        axios
          .post("/api/auth/login", data)
          .then((response) => {
            if (response.response && response.response.status === 404) {
              reject();
            }
            const data = response.data;
            Vue.prototype.$setCookie(
              "jwt",
              data.access_token,
              data.expires_in * 10
            );
            commit("setAuth", true);
            commit("setUser", data.user);
            Vue.prototype.$getCookie();
            resolve();
          })
          .catch(() => {
            reject();
          })
          .finally(() => {
            commit("setHideContent", false);
          });
      });
    },
    registerUser({ commit }, data) {
      return new Promise((resolve, reject) => {
        commit("setHideContent", true);
        axios
          .post("/api/auth/register", data)
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject();
          })
          .finally(() => {
            commit("setHideContent", false);
          });
      });
    },
    logoutUser({ commit }) {
      return new Promise((resolve, reject) => {
        commit("setHideContent", true);
        axios
          .post("/api/auth/logout")
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject();
          })
          .finally(() => {
            commit("setHideContent", false);
          });
      });
    },
    profileItem({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/profile/item")
          .then((response) => {
            commit("setProfileItem", response.data);
            resolve();
          })
          .catch(() => {
            reject();
          });
      });
    },
    profileItemById(_, id) {
      return new Promise((resolve, reject) => {
        axios
          .get(`api/profile/getuser/${id}`)
          .then((response) => {
            resolve(response.data);
          })
          .catch(() => {
            reject();
          });
      });
    },
    addSubscribe(_, id) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/profile/addtosub/${id}`)
          .then((response) => {
            resolve(response.data);
          })
          .catch(() => {
            reject();
          });
      });
    },
    deleteSubscribe(_, id) {
      return new Promise((resolve, reject) => {
        axios
          .post(`api/profile/deletefromsub/${id}`)
          .then((response) => {
            resolve(response.data);
          })
          .catch(() => {
            reject();
          });
      });
    },
    editProfileItem(_, data) {
      return new Promise((resolve, reject) => {
        axios
          .post("/api/profile/edit", data)
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject();
          });
      });
    },
    goalsItems({ commit }, payload = { limit: 12, page: 1 }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/goals/useritems/${payload.limit}?page=${payload.page}`)
          .then((response) => {
            commit("setGoalsItems", response.data);
            resolve();
          })
          .catch(() => {
            reject();
          });
      });
    },
    goalsItemsByUserId({ commit }, payload = { limit: 12, page: 1 }) {
      return new Promise((resolve, reject) => {
        axios
          .get(
            `api/goals/useritems/${payload.id}/${payload.limit}?page=${payload.page}`
          )
          .then((response) => {
            commit("setProfileGoals", response.data);
            resolve();
          })
          .catch(() => {
            reject();
          });
      });
    },
    loadAllGoals({ commit }, payload = { limit: 12, page: 1 }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/goals/allitems/${payload.limit}?page=${payload.page}`)
          .then((response) => {
            commit("setAllGoals", response.data);
            resolve();
          })
          .catch(() => {
            reject();
          });
      });
    },
    loadFavouritesGoals({ commit }) {
      axios.get("api/goals/allfav").then((response) => {
        commit("setFavouritesGoals", response.data);
      });
    },
  },
  modules: {},
});
